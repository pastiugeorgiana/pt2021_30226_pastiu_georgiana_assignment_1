package view;

import controller.Calculator;
import model.Polinom;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class View extends JFrame {
    private final JLabel P1 = new JLabel("Primul polinom:");
    private JTextField inp1 = new JTextField(40);
    private final JLabel P2 = new JLabel("Al 2-lea polinom:");
    private JTextField inp2 = new JTextField(40);
    private final JLabel res = new JLabel("Rezultat:");
    private JTextField outrez = new JTextField(40);
    private final JButton ad = new JButton("Adunare");
    private final JButton sub = new JButton("Scadere");
    private final JButton mul = new JButton("Inmultire");
    private final JButton der = new JButton("Derivare");
    private final JButton inte = new JButton("Integrare");
    private final JButton del = new JButton("Clear");

    String REZULTAT = null;
    Polinom P = new Polinom(), PP = new Polinom(), Prez = new Polinom();
    Calculator m = new Calculator();

    class ListenClasaAd implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent eveniment) {
            Prez.freeP();
            outrez.setText("");
            P.desfasurare = inp1.getText();
            PP.desfasurare = inp2.getText();
            REZULTAT = null;
            Pattern pattern = Pattern.compile("(-?\\d+)[x]\\^(-?\\d+)");

            Matcher matcher1 = pattern.matcher(P.desfasurare);
            while (matcher1.find()) {
                P.addMonomial(Integer.parseInt(matcher1.group(1)), Integer.parseInt(matcher1.group(2)));
            }

            Matcher matcher2 = pattern.matcher(PP.desfasurare);
            while (matcher2.find()) {
                PP.addMonomial(Integer.parseInt(matcher2.group(1)), Integer.parseInt(matcher2.group(2)));

            }
            Prez = m.addition(P, PP);
            REZULTAT = Prez.toString();
            outrez.setText("");
            outrez.setText(REZULTAT);
            REZULTAT = null;
            Prez.freeP();
            P.freeP();
            PP.freeP();
        }
    }

    class ListenClasaSub implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent eveniment) {
            Prez.freeP();
            outrez.setText("");
            P.desfasurare = inp1.getText();
            PP.desfasurare = inp2.getText();
            REZULTAT = null;
            Pattern pattern = Pattern.compile("(-?\\d+)[x]\\^(-?\\d+)");

            Matcher matcher1 = pattern.matcher(P.desfasurare);
            while (matcher1.find()) {
                P.addMonomial(Integer.parseInt(matcher1.group(1)), Integer.parseInt(matcher1.group(2)));
            }

            Matcher matcher2 = pattern.matcher(PP.desfasurare);
            while (matcher2.find()) {
                PP.addMonomial(Integer.parseInt(matcher2.group(1)), Integer.parseInt(matcher2.group(2)));
                ;
            }
            Prez = m.substraction(P, PP);
            REZULTAT = Prez.toString();
            outrez.setText("");
            outrez.setText(REZULTAT);
            REZULTAT = null;
            Prez.freeP();
            P.freeP();
            PP.freeP();
        }
    }

    class ListenClasaMul implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent eveniment) {
            Prez.freeP();
            outrez.setText("");
            P.desfasurare = inp1.getText();
            PP.desfasurare = inp2.getText();
            REZULTAT = null;
            Pattern pattern = Pattern.compile("(-?\\d+)[x]\\^(-?\\d+)");

            Matcher matcher1 = pattern.matcher(P.desfasurare);
            while (matcher1.find()) {
                P.addMonomial(Integer.parseInt(matcher1.group(1)), Integer.parseInt(matcher1.group(2)));
            }

            Matcher matcher2 = pattern.matcher(PP.desfasurare);
            while (matcher2.find()) {
                PP.addMonomial(Integer.parseInt(matcher2.group(1)), Integer.parseInt(matcher2.group(2)));
                ;
            }
            Prez = m.multiplication(P, PP);
            REZULTAT = Prez.toString();
            outrez.setText("");
            outrez.setText(REZULTAT);
            REZULTAT = null;
            Prez.freeP();
            P.freeP();
            PP.freeP();
        }
    }

    class ListenClasaDer implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent eveniment) {
            Prez.freeP();
            outrez.setText("");
            P.desfasurare = inp1.getText();
            REZULTAT = null;

            Pattern pattern = Pattern.compile("(-?\\d+)[x]\\^(-?\\d+)");

            Matcher matcher1 = pattern.matcher(P.desfasurare);
            while (matcher1.find()) {
                P.addMonomial(Integer.parseInt(matcher1.group(1)), Integer.parseInt(matcher1.group(2)));
            }

		/*
		PP.desfasurare=inp2.getText();Matcher matcher2 = pattern.matcher(PP.desfasurare);
		while (matcher2.find()) {
			PP.addMonomial(Integer.parseInt(matcher2.group(1)), Integer.parseInt(matcher2.group(2)));;
		}*/
            Prez = m.derivative(P);
            REZULTAT = Prez.toString();
            outrez.setText("");
            outrez.setText(REZULTAT);
            REZULTAT = null;
            Prez.freeP();
            P.freeP();
            //PP.freeP();
        }
    }

    class ListenClasaInte implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent eveniment) {
            Prez.freeP();
            outrez.setText("");
            P.desfasurare = inp1.getText();
            REZULTAT = null;
            Pattern pattern = Pattern.compile("(-?\\d+)[x]\\^(-?\\d+)");

            Matcher matcher1 = pattern.matcher(P.desfasurare);
            while (matcher1.find()) {
                P.addMonomial(Integer.parseInt(matcher1.group(1)), Integer.parseInt(matcher1.group(2)));
            }

		/*
		PP.desfasurare=inp2.getText();Matcher matcher2 = pattern.matcher(PP.desfasurare);
		while (matcher2.find()) {
			PP.addMonomial(Integer.parseInt(matcher2.group(1)), Integer.parseInt(matcher2.group(2)));;
		}*/
            Prez = m.integration(P);
            REZULTAT = Prez.toString();
            outrez.setText("");
            outrez.setText(REZULTAT);
            REZULTAT = null;
            Prez.freeP();
            P.freeP();
            //PP.freeP();
        }
    }

    class ListenClasaDel implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent eveniment) {
            outrez.setText("");
            inp1.setText("");
            inp2.setText("");
            REZULTAT = null;
            Prez.freeP();
            P.freeP();
            PP.freeP();
        }
    }

    public View() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 500);

        JPanel pn1 = new JPanel();
        pn1.add(P1);
        pn1.add(inp1);
        pn1.setLayout(new FlowLayout());

        JPanel pn2 = new JPanel();
        pn2.add(P2);
        pn2.add(inp2);
        pn2.setLayout(new FlowLayout());
        JPanel pn3 = new JPanel();
        pn3.add(ad);
        pn3.add(sub);
        pn3.add(mul);
        pn3.add(der);
        pn3.add(inte);
        pn3.add(del);

        JPanel pn4 = new JPanel();
        pn4.add(res);
        pn4.add(outrez);
        pn4.setLayout(new FlowLayout());

        JPanel pn = new JPanel();
        pn.add(pn1);
        pn.add(pn2);
        pn.add(pn3);
        pn.add(pn4);
        pn.add(new JLabel("! monomul de forma COEFICIENTx^GRAD !"));
        pn.setLayout(new BoxLayout(pn, BoxLayout.Y_AXIS));

        this.ListenAdd(new ListenClasaAd());
        this.ListenSub(new ListenClasaSub());
        this.ListenDer(new ListenClasaDer());
        this.ListenInte(new ListenClasaInte());
        this.ListenMul(new ListenClasaMul());
        this.ListenDel(new ListenClasaDel());
        this.add(pn);


    }

    void ListenMul(ActionListener ListenButon) {

        mul.addActionListener(ListenButon);
    }


    void ListenAdd(ActionListener ListenButon) {

        ad.addActionListener(ListenButon);
    }

    void ListenDel(ActionListener ListenButon) {

        del.addActionListener(ListenButon);
    }

    void ListenSub(ActionListener ListenButon) {

        sub.addActionListener(ListenButon);
    }

    void ListenDer(ActionListener ListenButon) {

        der.addActionListener(ListenButon);
    }

    void ListenInte(ActionListener ListenButon) {

        inte.addActionListener(ListenButon);
    }


}
