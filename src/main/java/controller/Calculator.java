package controller;

import model.Monomial;
import model.Polinom;

public class Calculator {

    public Polinom addition(Polinom p1, Polinom p2) {
        Polinom p11 = new Polinom(), p22 = new Polinom(), rez = new Polinom();

        for (Monomial mon : p1.poly)
            p11.addMonomial(mon.getCoef(), mon.getGrad());

        for (Monomial mon : p2.poly)
            p22.addMonomial(mon.getCoef(), mon.getGrad());

        for (Monomial unu : p11.poly)
            for (Monomial doi : p22.poly)
                if (unu.getGrad() == doi.getGrad()) {
                    float coef = unu.getCoef() + doi.getCoef();
                    doi.setGrad(-23);
                    unu.setCoef(coef);

                }

        for (Monomial unu : p11.poly)
            if (unu.getCoef() != 0)
                rez.addMonomial(unu.getCoef(), unu.getGrad());

        for (Monomial doi : p22.poly)
            if (doi.getGrad() > 0 && doi.getCoef() != 0)
                rez.addMonomial(doi.getCoef(), doi.getGrad());


        return rez;
    }


    public Polinom substraction(Polinom p1, Polinom p2) {
        Polinom p11 = new Polinom(), p22 = new Polinom(), rez = new Polinom();

        for (Monomial mon : p1.poly)
            p11.addMonomial(mon.getCoef(), mon.getGrad());

        for (Monomial mon : p2.poly)
            p22.addMonomial(mon.getCoef(), mon.getGrad());

        for (Monomial unu : p11.poly)
            for (Monomial doi : p22.poly)
                if (unu.getGrad() == doi.getGrad()) {
                    float coef = unu.getCoef() - doi.getCoef();
                    doi.setGrad(-23);
                    unu.setCoef(coef);

                }

        for (Monomial unu : p11.poly)
            if (unu.getCoef() != 0)
                rez.addMonomial(unu.getCoef(), unu.getGrad());

        for (Monomial doi : p22.poly)
            if (doi.getGrad() > 0 && doi.getCoef() != 0)
                rez.addMonomial(-doi.getCoef(), doi.getGrad());


        return rez;
    }


    public Polinom multiplication(Polinom p1, Polinom p2) {
        Polinom rez = new Polinom();

        for (Monomial unu : p1.poly)
            for (Monomial doi : p2.poly)
                rez.addMonomial((unu.getCoef() * doi.getCoef()), (unu.getGrad() + doi.getGrad()));

        return rez;

    }

    public Polinom derivative(Polinom p) {
        Polinom rez = new Polinom();

        for (Monomial unu : p.poly) {
            float coef = unu.getCoef() * unu.getGrad();
            int grad = unu.getGrad() - 1;
            if (coef != 0) rez.addMonomial(coef, grad);

        }

        return rez;
    }


    public Polinom integration(Polinom p) {
        Polinom rez = new Polinom();

        for (Monomial unu : p.poly) {
            float coef = unu.getCoef() / (unu.getGrad() + 1);
            int grad = unu.getGrad() + 1;
            if (coef != 0) rez.addMonomial(coef, grad);

        }

        return rez;
    }

}
