package model;

import model.Monomial;

import java.util.*;

public class Polinom {
    public Polinom() {
    }

    public List<Monomial> poly = new ArrayList<Monomial>();
    public String desfasurare;

    public Polinom(String input) {
        this.desfasurare = input;
    }

    public void addMonomial(float coef, int grad) {
        Monomial nou = new Monomial(coef, grad);

        nou.setCoef(coef);
        nou.setGrad(grad);

        poly.add(nou);

        Collections.sort(poly);
    }


    @Override
    public String toString() {
        String afisare = "";
        for (Monomial mon : poly) {
            if (mon.coef >= 0)
                afisare = afisare + "+" + String.valueOf(mon.coef) + "x^" + String.valueOf(mon.grad);
            else afisare = afisare + String.valueOf(mon.coef) + "x^" + String.valueOf(mon.grad);

        }

        return afisare;
    }


    public void freeP() {
        poly.clear();
        desfasurare = null;

    }

}
