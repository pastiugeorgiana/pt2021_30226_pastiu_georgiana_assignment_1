package model;

public class Monomial implements Comparable<Monomial> {

    public float coef;
    public int grad;

    Monomial(float coef, int grad) {
        this.coef = coef;
        this.grad = grad;
    }

    public float getCoef() {
        return coef;
    }

    public void setCoef(float coef) {
        this.coef = coef;
    }

    public int getGrad() {
        return grad;
    }

    public void setGrad(int grad) {
        this.grad = grad;
    }

    public void setMonomial(int deg, float coe) {
        grad = deg;
        coef = coe;
    }

    public int compareTo(Monomial termen2) {
        Integer deg = Integer.valueOf(this.getGrad());
        return deg.compareTo(termen2.getGrad()) * (-1);
    }
}
