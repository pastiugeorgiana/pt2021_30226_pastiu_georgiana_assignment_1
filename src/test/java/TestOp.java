import controller.Calculator;
import model.Polinom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestOp {

        @org.junit.Test
        public void testAd() {

            Calculator m = new Calculator();

            Polinom p1 = new Polinom();
            Polinom p2 = new Polinom();
            p1.addMonomial(1, 2);
            p1.addMonomial(3, 4);
            p2.addMonomial(5, 2);

            Polinom rez = new Polinom();
            rez.addMonomial(6,2);
            rez.addMonomial(3, 4);
            Polinom verif = new Polinom();

            verif = m.addition(p1, p2);

            if(rez.poly.size()==verif.poly.size())
                for (int i=0; i<rez.poly.size(); i++)
                {
                    assertTrue((int)rez.poly.get(i).getCoef()== (int)verif.poly.get(i).getCoef());
                    assertTrue(rez.poly.get(i).getGrad()==verif.poly.get(i).getGrad());
                }
            else assertEquals(rez.poly.size(),verif.poly.size());

        }

        @org.junit.Test
        public void testSub() {

            Calculator m = new Calculator();

            Polinom p1 = new Polinom();
            Polinom p2 = new Polinom();
            p1.addMonomial(1,1 );
            p1.addMonomial(1,0 );
            p2.addMonomial(1,1 );

            Polinom rez = new Polinom();
            rez.addMonomial(1,0);

            Polinom verif = new Polinom();

            verif = m.substraction(p1, p2);

            if(rez.poly.size()==verif.poly.size())
                for (int i=0; i<rez.poly.size(); i++)
                {
                    assertTrue((int)rez.poly.get(i).getCoef()== (int)verif.poly.get(i).getCoef());
                    assertTrue(rez.poly.get(i).getGrad()==verif.poly.get(i).getGrad());
                }
            else assertEquals(rez.poly.size(),verif.poly.size());

        }

        @org.junit.Test
        public void testMul() {

            Calculator m = new Calculator();

            Polinom p1 = new Polinom();
            Polinom p2 = new Polinom();
            p1.addMonomial(1, 2);
            p1.addMonomial(3, 4);
            p2.addMonomial(1, 2);

            Polinom rez = new Polinom();
            rez.addMonomial(1,4);
            rez.addMonomial(3,6);
            Polinom verif = new Polinom();

            verif = m.multiplication(p1, p2);

            if(rez.poly.size()==verif.poly.size())
                for (int i=0; i<rez.poly.size(); i++)
                {
                    assertTrue((int)rez.poly.get(i).getCoef()== (int)verif.poly.get(i).getCoef());
                    assertTrue(rez.poly.get(i).getGrad()==verif.poly.get(i).getGrad());
                }
            else assertEquals(rez.poly.size(),verif.poly.size());

        }

        @org.junit.Test
        public void testDer() {

            Calculator m = new Calculator();

            Polinom p1 = new Polinom();
            p1.addMonomial(1, 2);
            p1.addMonomial(3, 4);


            Polinom rez = new Polinom();
            rez.addMonomial(2,1);
            rez.addMonomial(12,3);
            Polinom verif = new Polinom();

            verif = m.derivative(p1);

            if(rez.poly.size()==verif.poly.size())
                for (int i=0; i<rez.poly.size(); i++)
                {
                    assertTrue((int)rez.poly.get(i).getCoef()== (int)verif.poly.get(i).getCoef());
                    assertTrue(rez.poly.get(i).getGrad()==verif.poly.get(i).getGrad());
                }
            else assertEquals(rez.poly.size(),verif.poly.size());

        }

        @org.junit.Test
        public void testInte() {

            Calculator m = new Calculator();

            Polinom p1 = new Polinom();
            p1.addMonomial(2, 1);

            Polinom rez = new Polinom();
            rez.addMonomial(1,2);

            Polinom verif = new Polinom();

            verif = m.integration(p1);

            if(rez.poly.size()==verif.poly.size())
                for (int i=0; i<rez.poly.size(); i++)
                {
                    assertTrue((int)rez.poly.get(i).getCoef()== (int)verif.poly.get(i).getCoef());
                    assertTrue(rez.poly.get(i).getGrad()==verif.poly.get(i).getGrad());
                }
            else assertEquals(rez.poly.size(),verif.poly.size());

        }

    }


